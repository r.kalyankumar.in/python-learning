from itertools import product

import requests.utils
import stripe
from reportlab.platypus import SimpleDocTemplate, Table, Paragraph, TableStyle
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet



from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from sqlalchemy import Column,String, Integer,ForeignKey,Text,VARCHAR,DateTime, Date, DATE, DATETIME
from datetime import datetime
from datetime import date
import math,random

import pymysql

pymysql.install_as_MySQLdb()
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:root@localhost/ShippingMgmt"

db = SQLAlchemy(app)
print("connection", db)

class administrator(db.Model):
    admin_id = Column(Integer, primary_key=True)
    admin_name = Column(String(30))
    admin_username = Column(String(15),unique=True)
    admin_password = Column(String(30))

class Sailemployee(db.Model):
    emp_id = Column(Integer, primary_key=True)
    emp_name = Column(String(30))
    emp_contact = Column(Integer)
    emp_address = Column(String(30))

class nonsailemployee(db.Model):
    emp_id = Column(Integer, primary_key=True)
    emp_name = Column(String(30))
    emp_contact = Column(Integer)
    emp_address = Column(String(30))

class Productlist(db.Model):
    # __table_args__ = {'schema': 'employeesystem'}
    id = Column(Integer,primary_key=True)
    name = Column(String(30))
    quantity = Column(Integer)
    price_per_item = Column(Integer)
    joinTable = relationship("Orderlist", backref="Productlist")


class Orderlist(db.Model):
    order_id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey("productlist.id"))
    customer_id = Column(Integer, ForeignKey("customerinfo.customer_id"))
    product_name = Column(String(30))
    product_quantity = Column(Integer)
    price = Column(Integer)
    customer_name = Column(String(30))
    purchase_time_stamb = Column(DATETIME)


class customerinfo(db.Model):
    customer_id = Column(Integer,primary_key=True)
    customer_name = Column(String(30))
    customer_contactno = Column(String(10))
    customer_email = Column(String(30))
    customer_addresss = Column(String(30))
    joinTable = relationship("Orderlist", backref="custmerinfo")


class Paymentmethod(db.Model):
    user_id = Column(Integer, primary_key=True)
    user_name = Column(String(30))
    user_location = Column(String(30))
    userto_location = Column(String(30))
    WireTransfers_amount = Column(Integer)
    joinTable = relationship("Recivedfund",backref="paymentmethod")

class Recivedfund(db.Model):
    To_id = Column(Integer,primary_key=True)
    Bank_name = Column(String(30))
    Bank_AvailableBalacne = Column(Integer)
    user_id = Column(Integer, ForeignKey("paymentmethod.user_id"), nullable=False)

class Sourcedesination(db.Model):
    ship_id = Column(Integer, primary_key=True)
    Ship_Name = Column(String(30))
    SourcePoint = Column(String(30))
    DestinationPoint = Column(String(30))
    During_the_day = Column(Integer)
    joinTable = relationship("Warehouse", backref="sourcedesination")

class Warehouse(db.Model):
    Warehouse_id = Column(Integer,primary_key=True)
    Warehouse_name = Column(String(30))
    Warehouse_product_name = Column(String(30))
    Warehouse_contact_number = Column(Integer)
    Warehouse_address =  Column(String(30))
    ship_id = Column(Integer, ForeignKey("sourcedesination.ship_id"))
    joinTable = relationship("Distributionlogistics", backref='warehouse')

class Distributionlogistics(db.Model):
    Logistics_id = Column(Integer,primary_key=True)
    Logistics_name = Column(String(30))
    Vechile_name = Column(String(30))
    Vechile_number = Column(String(30))
    Contact_person_name = Column(String(30))
    Contact_person_number = Column(Integer)
    Boarding_point = Column(String(30))
    Destination_point = Column(String(30))
    Warehouse_id = Column(Integer, ForeignKey("warehouse.Warehouse_id"))


def createTable():
    db.create_all()

# this method create for sailEmploee
@app.route('/sailuser', methods=["POST","GET"])
def sailEmployee():
    if request.method == "POST":
        new_post = Sailemployee(**request.json)
        db.session.add(new_post)
        db.session.commit()
        return "1 Row Affected"
    elif request.method == "GET":
        sendRequest = request.json
        tableRequest = Sailemployee.query.filter_by(emp_name=sendRequest.get("emp_name")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify("user information",convert_dict)
    else:
        return "not found check you methods"
# this method create for NonSailEmployee
@app.route('/nonsailuser', methods=["POST", "GET"])
def nonSailEmployee():
    if request.method == "POST":
        new_post = nonsailemployee(**request.json)
        db.session.add(new_post)
        db.session.commit()
        return "1 Row Affected"
    elif request.method =="GET":
        sendRequest = request.json
        tableRequest = nonsailemployee.query.filter_by(emp_id=sendRequest.get("emp_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify("user information",convert_dict)

@app.route('/productlist', methods=["POST", "GET"])
def productlist():
    if request.method == "POST":
        newRequest = Productlist(**request.json)
        db.session.add(newRequest)
        db.session.commit()
        return "product updated"
    elif request.method == "GET":
        sendRequest = request.json
        tableRequest = Productlist.query.filter_by(id=sendRequest.get("id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify("Available stock items  :",convert_dict)

@app.route('/customerinfo', methods=["POST", "GET"])
def customer():
    if request.method == "POST":
        newpost = customerinfo(**request.form)
        db.session.add(newpost)
        db.session.commit()
        return "customer data updated"
    elif request.method == "GET":
        sendRequest = request.json
        tableRequest = customerinfo.query.filter_by(customer_id=sendRequest.get("customer_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify("user information", convert_dict)


@app.route('/order', methods=["POST","GET"])
def order():
    if request.method == "GET":
        sendRequest = request.json

        user_info = db.session.query(customerinfo).filter_by(customer_id=sendRequest.get("customer_id")).first()
        print("UserInfo:",user_info.customer_name)

        product_info = db.session.query(Productlist).filter_by(id=sendRequest.get("product_id")).first()
        print("ProductInfo:", product_info.name)

        # db.session.add(user_info, product_info)
        product_qty = sendRequest.get("product_quantity")
        print("Product_qty:", product_qty, Date, datetime.now())

        order_detail = Orderlist(product_id = product_info.id, customer_id = user_info.customer_id,
                                  product_name = product_info.name, product_quantity = product_qty,
                                 price = product_info.price_per_item * product_qty,
                                 customer_name = user_info.customer_name, purchase_time_stamb = datetime.now())
        print('Order:', order_detail)

        db.session.add(order_detail)
        db.session.commit()
        return "1 Row Affected"

@app.route('/ordercustomer', methods=["GET"])
def orderGetQuery():

    sendRequest = request.json
    tableRequest = Orderlist.query.filter_by(customer_id=sendRequest.get("customer_id")).first()
    convert_dict = tableRequest.__dict__
    convert_dict.pop("_sa_instance_state")
    return jsonify("User order list:", convert_dict)



@app.route('/createpayment', methods=["POST", "GET"])
def payment():
    if request.method == "POST":
        tableRequest = Paymentmethod(**request.form)
        db.session.add(tableRequest)
        db.session.commit()
        return "Payment Successfully"
    elif request.method == "GET":
        sendRequest = request.json
        tableRequest = Paymentmethod.query.filter_by(user_id=sendRequest.get("user_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify("User Payment list:", convert_dict)
    else:
        pass

@app.route('/recivedfund', methods=["POST","GET"])
def recivedfund():
    if request.method == "POST":
        tableRequest = Recivedfund(**request.form)
        db.session.add(tableRequest)
        db.session.commit()
        return "Payment Sccessfully"
    else:
        pass


@app.route('/source', methods=["GET","POST"])
def sourceDestination():
    if request.method =="POST":
        tableRequest = Sourcedesination(**request.form)
        db.session.add(tableRequest)
        db.session.commit()
        return "1 row affected"
    elif request.method == "GET":
        sendRequest = request.form
        tableRequest = Sourcedesination.query.filter_by(ship_id=sendRequest.get("ship_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify(convert_dict)
    else:
        pass

@app.route('/track', methods=["GET"])
def track():
    sendRequest = request.json
    joinTwoTable = db.session.query(customerinfo,Orderlist).filter(
        customerinfo.customer_id == Orderlist.customer_id).filter(
        Orderlist.customer_id == sendRequest.get("customer_id")
    ).all()
    emptylist = []
    for i in joinTwoTable:
        if i == sendRequest.get("customer_id"):
            assgin = i.Orderlist
            change_to_dict = assgin.__dict__
            change_to_dict.pop("_sa_instance_state")
            emptylist.append(assgin)
        # return jsonify(f'customer_id: {i.Orderlist.customer_id}, customer_name: {i.Orderlist.customer_name},'
        #         f'product_name: {i.Orderlist.product_name}, product_quantity: {i.Orderlist.product_quantity},'
        #         f'price: {i.Orderlist.price}, OrderId: {i.Orderlist.order_id}, '
        #         f'Order_date: {i.Orderlist.purchase_time_stamb},')
        return jsonify(emptylist)
    else:
        return "not found"

@app.route('/Warehouse', methods=["POST", "GET", "DELETE", "PUT"])
def wareHouse():
    if request.method == "POST":
        tableRequest = Warehouse(**request.form)
        db.session.add(tableRequest)
        db.session.commit()
        return "1 row affected, warehouse data updated"
    elif request.method == "GET":
        sendRequest = request.form
        tableRequest = Warehouse.query.filter_by(Warehouse_id=sendRequest.get("Warehouse_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify(convert_dict)
    elif request.method == "PUT":
        sendRequest = request.form
        tableRequest = Warehouse.query.filter_by(Warehouse_product_name=sendRequest.get("Warehouse_product_name")).update(
            { Warehouse.Warehouse_name : sendRequest}
        )
        db.session.add(tableRequest)
        db.session.commit()
        return "one row update"


@app.route('/logistics', methods=["POST", "GET",])
def logistics():
    # if request.method == "GET":
    #     sendRequest = request.json
    #     ship_info = db.session.query(Sourcedesination).filter_by(ship_id=sendRequest.get("ship_id")).first()
    #     print("ship_info", ship_info)
    #
    #     warehouse_info = db.session.query(Warehouse).filter_by(Warehouse_id=sendRequest.get("Warehouse_id")).first()
    #     print("warehouse_info",warehouse_info.Warehouse_name)
    #
    #     logistics_info = db.session.query(Distributionlogistics).filter_by(Logistics_id=sendRequest.get("Logistics_id")).first()
    #     print("logistics_info",logistics_info.Logistics_name)
    #
    #     order_details = Distributionlogistics(Logistics_id = logistics_info.Logistics_id, Logistics_name= logistics_info.Logistics_name,
    #                                           Vechile_name = Distributionlogistics.Vechile_name, Vechile_number = Distributionlogistics.Vechile_number,
    #                                           Contact_person_name = Distributionlogistics.Contact_person_name, Contact_person_number = Distributionlogistics.Contact_person_number,
    #                                           Boarding_point = Distributionlogistics.Boarding_point, Destination_point = Distributionlogistics.Destination_point,
    #                                           Warehouse_id = warehouse_info.Warehouse_id)
    #     print("order_details", order_details)
    #     db.session.add(order_details)
    #     db.session.commit()
    #     return "1 row affected, Logistics data updated"

    if request.method == "POST":
        sendRequest = request.form
        tableRequest = Distributionlogistics.query.filter_by(Logistics_id=sendRequest.get("Logistics_id")).first()
        convert_dict = tableRequest.__dict__
        convert_dict.pop("_sa_instance_state")
        return jsonify(convert_dict)
    else:
        pass




@app.route('/invoice',methods=["GET"])
def gendrateInvoice():
    DATA = [
        ["Date", "Name", "Subscription", "Price (Rs.)"],
        [
            "07/10/2021",
            "Car",
            "Lifetime",
            "10,999.00/-",
        ],
        ["08/10/2021", "", "6 months", "19,999.00/-"],
        ["Sub Total", "", "", "20,9998.00/-"],
        ["Discount", "", "", "-3,000.00/-"],
        ["Total", "", "", "17,998.00/-"],
    ]
    pdf = SimpleDocTemplate("receipt.pdf", pagesize=A4)
    styles = getSampleStyleSheet()
    title_style = styles["Heading1"]
    title_style.alignment = 1
    title = Paragraph("KK Logistics", title_style)
    style = TableStyle(
        [
            ("BOX", (0, 0), (-1, -1), 1, colors.black),
            ("GRID", (0, 0), (4, 4), 1, colors.black),
            ("BACKGROUND", (0, 0), (3, 0), colors.gray),
            ("TEXTCOLOR", (0, 0), (-1, 0), colors.whitesmoke),
            ("ALIGN", (0, 0), (-1, -1), "CENTER"),
            ("BACKGROUND", (0, 1), (-1, -1), colors.beige),
        ]
    )
    table = Table(DATA, style=style)
    pdf.build([title, table])


    return "Done invoice copy generator"





if __name__ == '__main__':
    createTable()

    app.run(port=7987, debug=True)
