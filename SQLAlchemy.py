

from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import update
import pymysql
pymysql.install_as_MySQLdb()
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://root:@localhost/test"
print(app.config,"connected")
db = SQLAlchemy(app)



class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    age = db.Column(db.Integer)
    contact = db.Column(db.Integer)
    email = db.Column(db.String(50))
    salary = db.Column(db.Integer)

    # def __init__(self,name, age, contact, email,salary):
    #     self.name = name
    #     self.age = age
    #     self.contact = contact
    #     self.email = email
    #     self.salary = salary

def create_table():  # This function directly created in database
    db.create_all()

@app.route('/insertData', methods=["POST"])
def add_user():
    insert_newRecord = Employee(**request.json)
    db.session.add(insert_newRecord)
    db.session.commit()
    print(insert_newRecord)
    return "Data Added Successfully"

@app.route('/updateQuery', methods=["PUT"])
def update_user():
    if Employee.name == Employee.name:
        search = request.json
        updateQuery = Employee.query.filter_by(id=search.get("id")).update(dict(name=search.get("name")))
        print(updateQuery)
        db.session.commit()
    elif Employee.contact == Employee.contact:
        search = request.json
        updateQuery = Employee.query.filter_by(id=search.get("id")).update(dict(contact=search.get("contact")))
        print(updateQuery)
        db.session.commit()
    elif Employee.salary == Employee.salary:
        search = request.json
        updateQuery = Employee.query.filter_by(id=search.get("id")).update(dict(salary=search.get("salary")))
        print(updateQuery)
        db.session.commit()
    return "Data updated Successfully"


@app.route('/getQuery', methods=["GET"])
def getQuery():
    search = request.json#has full request object from Postman
    print("check",search)
    userGetQuery = Employee.query.filter_by(name=search.get("name")).all()
    emptylist = []
    for user in userGetQuery:
        new_user = {}
        new_user['id'] = user.id
        new_user['name'] = user.name
        new_user['email'] = user.email
        print('new User: ', new_user)
        emptylist.append(new_user)
    return jsonify(emptylist)


@app.route("/salarySummary", methods=["GET"])
def minSalary():
    allUserData = Employee.query.all()
    allEmployeeSalary = []

    for user in allUserData:
        print('salary of user:', user.salary)
        allEmployeeSalary.append(user.salary)

    min_salary = min(allEmployeeSalary)
    print('Min User salary', min_salary);

    max_salary = max(allEmployeeSalary)
    print('Max User salary', max_salary);

    return jsonify({'Min Salary': min_salary, 'Max Salary': max_salary})


if __name__ == '__main__':
    # create_table()
    app.run(port=7896)
    print(db,"connection added")


