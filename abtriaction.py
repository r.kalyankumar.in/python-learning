# # Abtractions
# # ABC  ===> Abstract Base Class
# # What is abstract class.? == > class with at least one abstract method.

from abc import *
class Indian(ABC):
    @abstractmethod
    def havingBreakfast(self):
        pass
class Tamils(Indian):
    def havingDineer(self):
        print("Eating Pongal, idle")
class Marati(Indian):
    def havingBreakfast(self):
        print("Vada pav")

t = Tamils()
from abc import *
class DBConnect(ABC):
    @abstractmethod
    def establishdb(self):
        pass
    @abstractmethod
    def disconnect(self):
        pass

class OracleDB(DBConnect):
    def establishdb(self):
        print("Oracle DB Connected")

    def disconnect(self):
        print("Oracle DB Disconnect")

class MYSQLDB(DBConnect):
    def establishdb(self):
        print("MYSQL DB Connected")

    def disconnect(self):
        print("MYSQL DB Disconnect")

DBName = input("Enter the Class Name: ")
className = globals()[DBName]
c = className()
c.establishdb()
c.disconnect()

