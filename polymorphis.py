# Polymorphis Method overloading
# What is polymorphis
# Same type or method but different signatures

class Grandparent():
    def show(self):
        print("It's GrandParent")
class parent(Grandparent):
    def show(self):
        print("It's Parent")
class child(parent):
    def show(self):
        print("It's child")

obj1=child()
obj2=parent()
obj3=Grandparent()
obj1.show()
obj2.show()
obj3.show()
